package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type BlockResult struct {
	Result struct {
		Block struct {
			Header struct {
				Height string    `json:"height"`
				Time   time.Time `json:"time"`
			} `json:"header"`
		} `json:"block"`
	} `json:"result"`
}

type NetInfoResult struct {
	Result struct {
		NPeers string `json:"n_peers"`
	} `json:"result"`
}

var (
	highestBlockNumber = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "cosmos_highest_block_number",
		Help: "The highest block number in the Cosmos blockchain",
	})
	blockTimeDrift = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "cosmos_block_time_drift_seconds",
		Help: "The difference between current time and block creation time in seconds",
	})
	connectedPeers = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "cosmos_connected_peers",
		Help: "The number of peers connected to the Cosmos node",
	})
)

func init() {
	prometheus.MustRegister(highestBlockNumber)
	prometheus.MustRegister(blockTimeDrift)
	prometheus.MustRegister(connectedPeers)
}

func main() {
	cosmosNodeEndpoint := os.Getenv("COSMOS_EXPORTER_RPC_URL")
	if cosmosNodeEndpoint == "" {
		cosmosNodeEndpoint = "http://localhost:26657"
	}

	listenAddr := os.Getenv("COSMOS_EXPORTER_LISTEN_ADDR_PORT")
	if listenAddr == "" {
		listenAddr = "127.0.0.1:8080"
	}

	go func() {
		for {
			updateMetrics(cosmosNodeEndpoint)
			time.Sleep(15 * time.Second)
		}
	}()

	http.Handle("/metrics", promhttp.Handler())
	log.Printf("Starting Cosmos Prometheus exporter on %s", listenAddr)
	log.Fatal(http.ListenAndServe(listenAddr, nil))
}

func updateMetrics(endpoint string) {
	updateBlockMetrics(endpoint)
	updatePeerMetrics(endpoint)
}

func updateBlockMetrics(endpoint string) {
	resp, err := http.Get(fmt.Sprintf("%s/block", endpoint))
	if err != nil {
		log.Printf("Error fetching block data: %v", err)
		return
	}
	defer resp.Body.Close()

	var blockResult BlockResult
	if err := json.NewDecoder(resp.Body).Decode(&blockResult); err != nil {
		log.Printf("Error decoding block data: %v", err)
		return
	}

	height, err := stringToFloat64(blockResult.Result.Block.Header.Height)
	if err != nil {
		log.Printf("Error converting block height: %v", err)
		return
	}
	highestBlockNumber.Set(height)

	blockTime := blockResult.Result.Block.Header.Time
	drift := time.Since(blockTime).Seconds()
	blockTimeDrift.Set(drift)
}

func updatePeerMetrics(endpoint string) {
	resp, err := http.Get(fmt.Sprintf("%s/net_info", endpoint))
	if err != nil {
		log.Printf("Error fetching net info: %v", err)
		return
	}
	defer resp.Body.Close()

	var netInfoResult NetInfoResult
	if err := json.NewDecoder(resp.Body).Decode(&netInfoResult); err != nil {
		log.Printf("Error decoding net info: %v", err)
		return
	}

	peers, err := stringToFloat64(netInfoResult.Result.NPeers)
	if err != nil {
		log.Printf("Error converting peers count: %v", err)
		return
	}
	connectedPeers.Set(peers)
}

func stringToFloat64(s string) (float64, error) {
	var f float64
	_, err := fmt.Sscanf(s, "%f", &f)
	return f, err
}