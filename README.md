# cosmos-validator-exporter

## Configure Prometheus to scrape exporter

Add your exporter to a Prometheus configuration file (prometheus.yml)
```yaml
scrape_configs:
  - job_name: 'cosmos'
    static_configs:
      - targets: ['localhost:8080']
```

Run Prometheus with this configuration.
```bash
prometheus --config.file=prometheus.yml
```